<?php
/**
 * Plugin Name: Magnific Popup WordPress for PSDtoWPservice
 * Plugin URI: https://bitbucket.org/devshuvo/magnific-popup-wordpress
 * Bitbucket Plugin URI: https://bitbucket.org/devshuvo/magnific-popup-wordpress
 * Description: Magnific Popup is a responsive lightbox & dialog script with focus on performance and providing best experience for user with any device. <code>[yt_popup href="link"]content[/yt_popup]</code> <code>[vm_popup href="link"]content[/vm_popup]</code> <code>[gm_popup href="link"]content[/gm_popup]</code> <code>[img_popup href="link"]content[/img_popup]</code>
 * Author: 
 * Text Domain: mfwp
 * Domain Path: /lang/
 * Author URI: 
 * Tags: 
 * Version: 1.0.0
 */

// don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}


/**
 *	Enqueue Wooinstant scripts
 *
 */
function mfwp_enqueue_scripts(){
	//Stylesheet
	wp_enqueue_style('magnific-popup', plugin_dir_url( __FILE__ ).'assets/css/magnific-popup.css' );	
	
	//JS
	wp_enqueue_script('magnific-popup', plugin_dir_url( __FILE__ ).'assets/js/jquery.magnific-popup.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script('mfwp-scripts', plugin_dir_url( __FILE__ ).'assets/js/mfwp-scripts.js', array( 'jquery' ), '', true );


}
add_filter( 'wp_enqueue_scripts', 'mfwp_enqueue_scripts', 199 );


/*YT Popup Function*/
function mfwp_yt_shortcode( $atts, $content = null ) {       
    // Params extraction
    extract(
        shortcode_atts(
            array(
                'href'  => ''
            ), 
            $atts
        )
    );

	$html = '<a class="popup-youtube" href="' . $href . '">' . do_shortcode($content) . '</a>';

    return $html;
}
add_shortcode('yt_popup','mfwp_yt_shortcode');

/*Vimeo Popup Function*/
function mfwp_vimeo_shortcode( $atts, $content = null ) {       
    // Params extraction
    extract(
        shortcode_atts(
            array(
                'href'  => ''
            ), 
            $atts
        )
    );

	$html = '<a class="popup-vimeo" href="' . $href . '">' . do_shortcode($content) . '</a>';

    return $html;
}
add_shortcode('vm_popup','mfwp_vimeo_shortcode');

/*Google Map Popup Function*/
function mfwp_gm_shortcode( $atts, $content = null ) {       
    // Params extraction
    extract(
        shortcode_atts(
            array(
                'href'  => ''
            ), 
            $atts
        )
    );

	$html = '<a class="popup-gmaps" href="' . $href . '">' . do_shortcode($content) . '</a>';

    return $html;
}
add_shortcode('gm_popup','mfwp_gm_shortcode');

/*Single image lightbox*/

function mfwp_simple_img_lightbox_shortcode( $atts, $content = null ) {       
    // Params extraction
    extract(
        shortcode_atts(
            array(
                'href'  => ''
            ), 
            $atts
        )
    );

	$html = '<a class="image-popup-vertical-fit" href="' . $href . '">' . do_shortcode($content) . '</a>';

    return $html;
}
add_shortcode('img_popup','mfwp_simple_img_lightbox_shortcode');